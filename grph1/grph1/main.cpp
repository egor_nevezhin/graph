#define _CRT_SECURE_NO_DEPRECATE

#include <iostream>
#include <string>
#include <vector>
#include <queue>

using namespace std;

class Vertex{
private:
	string name;
public:
	bool operator == (const Vertex& left) {
		return left.name == this->name;
	}
	Vertex(string name){
		this->name = name;
	}
	~Vertex(){};
	string getName() const{
		return name;
	}
};

class Edge{
private:
	Vertex beg, end;
	//double length;
public:
	Edge(Vertex beg, Vertex end) : beg(beg), end(end) {};

	~Edge(){};

	Vertex getBegin() const{
		return beg;
	}

	Vertex getEnd() const{
		return end;
	}
	
	void setBegin(Vertex beg){
		this->beg = beg;
	}

	void setEnd(Vertex end){
		this->end = end;
	}

};

class Graph{
private:
	vector<Edge> edges;
	vector<Vertex> vertex;
	bool directed;

	queue <string> connectedVertex(bool * visited, string ver) {
		queue <string> tmp;

		for (int i = 0; i < this->getEdges().size(); i++) {
			if (this->isDirected()) {
				if (this->edges[i].getBegin().getName() == ver && !visited[this->find(this->edges[i].getEnd().getName())]) {
					tmp.push(this->edges[i].getBegin().getName());

				}
			}
			else {
 				if (this->edges[i].getBegin().getName() == ver && !visited[this->find(this->edges[i].getEnd().getName())]) {
					tmp.push(this->edges[i].getEnd().getName());
					continue;
				}
				if (this->edges[i].getEnd().getName() == ver && !visited[this->find(this->edges[i].getBegin().getName())]) {
					tmp.push(this->edges[i].getBegin().getName());
					continue;
				}
			}
		}

		return tmp;
	}

public:
	Graph(bool directed = false){
		this->directed = directed;
	}

	~Graph(){};
	bool insert(Edge edge){
		edges.push_back(edge);
		return true;
	}

	vector <Edge> getEdges(){
		return edges;
	}

	vector <Vertex> getVertex(){
		return vertex;
	}

	bool isDirected(){
		return directed;
	}

	void setDirected(bool directed){
		this -> directed = directed;
	}

	void insertVertex(string name){
		Vertex tmp(name);
		vertex.push_back(tmp);
	}

	int find(string name) {
		for (int i = 0; i < vertex.size(); i++) {
			if (vertex[i].getName() == name) {
				return i;
			}
		}
		return -1;
	}

	void insertEdge(string n1, string n2){
		int tmp1 = find(n1);
		int tmp2 = find(n2);
		if (tmp1 != -1 && tmp2 != -1){
			Edge tempEdge(vertex[tmp1], vertex[tmp2]);
			edges.push_back(tempEdge);
		}
	}

	
	void remove(Vertex v1) { // let's see it!! and rewrite
		vertex.erase(vertex.begin() + find(v1.getName()), vertex.begin() + find(v1.getName()) + 1);
		for (int i = 0; i < edges.size(); i++){
			if (edges[i].getBegin() == v1 || edges[i].getEnd() == v1){
				edges.erase(edges.begin() + i, edges.begin() + i + 1);
			}
		}
	}
	// write remove edge

	void bfs(string start, string end) {
		cout << "BFS start at: " << start << " and end in: " << end;
		bool * visited = new bool[this->edges.size()];
		for (int i = 0; i < this->edges.size(); i++){
			visited[i] = false;
		}
		queue <string> q;

		q.push(start);
		visited[this->find(start)] = true;
		while (!q.empty()) {
			string node = q.front();
			q.pop();


			if (node == end) {
				cout << " connected." << endl;
				return;
			}
			queue <string> cv = connectedVertex(visited, node);
			long queueSize = cv.size();
			for (int i = 0; i < queueSize; i++) {
				q.push(cv.front());
				visited[this->find(cv.front())] = true;
				cv.pop();
			}
		}
		cout << " not connected." << endl;
	}

};

class IO {
private:

public:
	// ����� ����� � �������.
	void show(Graph &g) {
		cout << "Graph: ";
		if (g.isDirected())
			cout << "Directed";
		else
			cout << "Undirected";

		cout << endl << "Vertexes" << endl;
		for (int i = 0; i < g.getVertex().size(); i++) {
			cout << g.getVertex()[i].getName() << " ";
		}
		cout << endl;

		cout << "Edges" << endl;
		for (int i = 0; i < g.getEdges().size(); i++) {
			cout << "(" << g.getEdges()[i].getBegin().getName() << "; " << g.getEdges()[i].getEnd().getName() << ")" << endl;
		}
	}

	void scanFromFile(Graph &g, const char * path) {
		FILE* file = fopen(path, "r");
		if (file == NULL) {
			cout << "File doesn't exist! Aborting." << endl;
			return;
		}

		char tmp;
		string line;
		int n = 0;

		do {
			tmp = getc(file);
			if (tmp != '\n' && tmp != EOF) {
				line += tmp;
				n++;
			}
			else {
				if (line == "directed") {
					g.setDirected(true);
				}
				else if (line == "undirected") {
					g.setDirected(false);
				}
				else {

					string begin = line.substr(0, line.find(" "));
					string end = line.substr(line.find(" ") + 1, line.size() - 1);
					if (g.find(begin) < 0) {
						g.insertVertex(begin);
					}

					if (g.find(end) < 0) {
						g.insertVertex(end);
					}
					g.insertEdge(begin, end);

				}

				line = "";
				n = 0;
			}


		} while (tmp != EOF);

		fclose(file);

	}
};

int main(){
	Graph * G = new Graph();
	IO * io = new IO();
	io->scanFromFile(*G, "input.txt");
	io->show(*G);
	G->bfs("1","8");
	return 0;
}